package org.btssio.siogel;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class AfficheSignature extends AppCompatActivity implements View.OnClickListener {

    ClientDAO dao = new ClientDAO(this);
    LinearLayout vue_sign;
    Button C_sing_annuler;
    ImageView image;
    Client client;
    Signature laSignature;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_affiche_signature);

        int id = this.getIntent().getExtras().getInt("id");
        client = dao.getClient(id);

        image = (ImageView) findViewById(R.id.imagesignature);
        C_sing_annuler = (Button) findViewById(R.id.C_sign_annuler);
        C_sing_annuler.setOnClickListener(this);

        laSignature = new AfficheSignature.Signature(this,null);
        laSignature.dessine(client.getSignatureBase64());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.C_sign_annuler:
                finish();
                break;

        }
    }
    public class Signature extends View {
        // variables nécessaire au dessin
        private Bitmap bitmap;
        // constructeur
        public Signature(Context context, AttributeSet attrs) {
            super(context, attrs);
        }
        public void dessine(String encodedString) {
            try {
                byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
                bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
// puis « écriture » de l’image bitmap obtenue dans le composant correspondant du Layout en cours
// (dans notre cas une ImageView)
                image.setImageBitmap(bitmap);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "error dessine",
                        Toast.LENGTH_LONG).show();
            }
        }
    }

}