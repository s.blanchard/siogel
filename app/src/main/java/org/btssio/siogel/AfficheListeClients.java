package org.btssio.siogel;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class AfficheListeClients extends Activity implements AdapterView.OnItemClickListener{
    Intent ModifClents;
    ListView listView;
    ClientAdapter clientAdapter;
    List<Client> listeClients = new ArrayList<Client>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_affiche_liste_clent);
        ClientDAO dao = new ClientDAO(this);
        listeClients = dao.getAll();
        
        listView = (ListView)findViewById(R.id.lvListe);
        clientAdapter = new ClientAdapter(this, listeClients);
        listView.setAdapter(clientAdapter);

        listView.setOnItemClickListener(this);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        ModifClents = new Intent(this, ModificationClient.class);
        ModifClents.putExtra("id", listeClients.get(position).getIdentifiant());
        this.startActivity(ModifClents);

        //Toast.makeText(getApplicationContext(),"Choix : "+
                //listeClients.get(position).getIdentifiant(), Toast.LENGTH_LONG).show();
    }
}