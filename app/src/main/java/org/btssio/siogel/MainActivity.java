package org.btssio.siogel;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity implements android.view.View.OnClickListener {
    private ImageButton log;
    private ImageButton delivery;
    private ImageButton save;
    private ImageButton importData;

    Intent intentClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        log= (ImageButton) findViewById(R.id.imageButton);
        log.setOnClickListener(this);
        delivery= (ImageButton) findViewById(R.id.imageButton2);
        delivery.setOnClickListener(this);
        save= (ImageButton) findViewById(R.id.imageButton3);
        save.setOnClickListener(this);
        importData= (ImageButton) findViewById(R.id.imageButton4);
        importData.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.imageButton:
                //TODO
                break;
            case R.id.imageButton2:
                //intentClient.putExtra()
                intentClient = new Intent(this, AfficheListeClients.class);
                this.startActivity(intentClient);
                break;
            case R.id.imageButton3:
                //TODO
                break;
            case R.id.imageButton4:
                //TODO
                break;
        }
    }
}