package org.btssio.siogel;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

import android.content.Context;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static android.location.Criteria.ACCURACY_FINE;
import static android.location.Criteria.POWER_HIGH;

public class Geolocalisation extends FragmentActivity implements OnMapReadyCallback, LocationListener  {

    private GoogleMap mMap;
    private LocationManager locationManager;
    private String fournisseur;
    private LatLng positionClient, positionLivreur;
    private Boolean geoLocLivreur = false, geoLocClient = false;
    private LatLngBounds.Builder builder;
    private SupportMapFragment mapFragment;
    private Criteria criteres;
    private String adresse, cp, ville;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geolocalisation);
        adresse = this.getIntent().getExtras().getString("adresse");
        cp = this.getIntent().getStringExtra("cp");
        ville = this.getIntent().getStringExtra("ville");
        Log.d("test ", adresse + " " + cp +" "+ ville);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        builder = new LatLngBounds.Builder();
        recupPositionLivreur();
        recupPositionClient();
        mapFragment.getMapAsync(this);

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        //LatLng sydney = new LatLng(-34, 151);
        if(geoLocClient){
            mMap.addMarker(new MarkerOptions().position(positionClient).title("position client"));
        }


        if (geoLocLivreur){
            mMap.addMarker(new MarkerOptions().position(positionLivreur).title("position livreur"));
        }


        //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }

    public void recupPositionLivreur(){
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteres = new Criteria();
        // Précision pour la localisation (Fine ou "grossière")
        criteres.setAccuracy(ACCURACY_FINE);
        // Conso d'énergie
        criteres.setCostAllowed(true);
        criteres.setPowerRequirement(POWER_HIGH);

        // Recherche du meilleur provider / fournisseur en fonction des critères, et récupération de la position :
        fournisseur = locationManager.getBestProvider(criteres, true);
        if(fournisseur == null || fournisseur.isEmpty()) { // si aucun fournisseur n'a été trouvé
            Toast.makeText(this, "Géolocalisation impossible", Toast.LENGTH_SHORT).show();
        }
        else {
            /* un fournisseur a été trouvé, il faut donc gérer la maj de la géolocalisation
            avec la méthode requestLocationUpdates qui prend en paramètres :
            - le fournisseur de position
            - la période entre 2 maj en millisecondes
            - la période entre 2 maj en mètres
            - l'écouteur qui sera lancé dès que le fournisseur sera activé
            */
            try {
                locationManager.requestLocationUpdates(fournisseur, 20000, 0, (LocationListener) this);
                // Dernière localisation connue
                Location location = locationManager.getLastKnownLocation(fournisseur);
                if(location != null){
                    // récup coordonnées
                    positionLivreur = new LatLng(location.getLatitude(), location.getLongitude());
                    geoLocLivreur = true;
                }
                else{
                    // Fausses coordonnées - par défaut
                    positionLivreur = new LatLng(47.958423, 0.166806);
                    geoLocLivreur = true;
                    Toast.makeText(this, "Erreur de localisation, coordonnées par défaut affichée",
                            Toast.LENGTH_LONG).show();
                }
            } catch (SecurityException e) {
                geoLocLivreur = false;
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }
    public void recupPositionClient(){
        Geocoder geoCoder = new Geocoder(this, Locale.FRANCE);



        try {
            List<Address> addressList = geoCoder.getFromLocationName(adresse+"," + cp + "," + ville + " France",10);
            Address address = addressList.get(0);
            positionClient = new LatLng(address.getLatitude(), address.getLongitude());
            geoLocClient = true;

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onLocationChanged(@NonNull Location location) {

    }
}