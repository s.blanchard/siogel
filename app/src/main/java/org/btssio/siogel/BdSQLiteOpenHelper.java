package org.btssio.siogel;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class BdSQLiteOpenHelper extends SQLiteOpenHelper {

    private String requete="create table client ("
            + "idC INTEGER PRIMARY KEY AUTOINCREMENT,"
            + "nomC TEXT NOT NULL,"
            + "prenomC TEXT NOT NULL,"
            + "adresse TEXT NOT NULL,"
            + "codepostal INTEGER NOT NULL,"
            + "ville TEXT NOT NULL,"
            + "telephone INTEGER NOT NULL,"
            + "dateCommande TEXT NOT NULL,"
            + "heureSouhaitLiv TEXT NOT NULL,"
            + "heureReelleLivraison TEXT,"
            + "dateLivraison TEXT,"
            + "signatureBase64 TEXT,"
            + "etat INTEGER"
            + ");";


    public BdSQLiteOpenHelper(Context context, String name, CursorFactory factory, int version) {
        super(context, name, factory, version);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub

        db.execSQL(requete);

        db.execSQL("insert into client (nomC,prenomC,adresse, codepostal,ville,telephone,dateCommande,heureSouhaitLiv) values('Jean','Kirstein','40 Avenue de Clichy',75018,'Paris',0142930472,'17-12-2020','12:00');");
        db.execSQL("insert into client (nomC,prenomC,adresse, codepostal,ville,telephone,dateCommande,heureSouhaitLiv) values('Conny','GrosseDarone','99, rue des Dunes',35400,'Saint-Malo',0603982531,'19-11-2020','15:00');");
        db.execSQL("insert into client (nomC,prenomC,adresse, codepostal,ville,telephone,dateCommande,heureSouhaitLiv) values('Jake','Peralta','465 6th Avenue',11217,'NY',911,'15-10-2020','17:00');");
        db.execSQL("insert into client (nomC,prenomC,adresse, codepostal,ville,telephone,dateCommande,heureSouhaitLiv) values('Ronald','Kemble','99, rue des Nations Unies',42400,'SAINT-CHAMOND',0142930472,'16-12-2020','06:00');");
        db.execSQL("insert into client (nomC,prenomC,adresse, codepostal,ville,telephone,dateCommande,heureSouhaitLiv) values('Bradley','Zubia','92, avenue du Marechal Juin',68300,'SAINT-LOUIS',0142930472,'18-01-2021','10:00');");
        /*
        ContentValues value = new ContentValues();
        value.put("nomV", "Badoz");
        value.put("niveauV",50);

        db.insert("client", null, value);
        */
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS client");
        Log.i("Test","test");
        onCreate(db);
    }

}
